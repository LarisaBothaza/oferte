package ubb.service;

import ubb.domain.Rezervare;
import ubb.domain.Rezervare;
import ubb.repository.Repository;
import ubb.utils.events.SchimbareEvent;
import ubb.utils.observer.Observable;
import ubb.utils.observer.Observer;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class RezervareService implements Observable<SchimbareEvent> {
    private final Repository<Long, Rezervare> repoRezervare;
    private List<Observer<SchimbareEvent>> observers = new ArrayList<>();

    public RezervareService(Repository<Long, Rezervare> repoRezervare) {
        this.repoRezervare = repoRezervare;
    }

    /**
     *saves the user received parameter
     * @param MembruParam
     * @return the user created
     */
    public Rezervare addRezervare(Rezervare MembruParam) {
        Rezervare Rezervare = repoRezervare.save(MembruParam);
        //validatorMembruService.validateAdd(Membru);
        return Rezervare;
    }

    ///TO DO: add other methods

    /**
     *delete the user identified by the received id, and also delete that user's friendships
     * @param id long - id's to be deleted
     * @return deleted user
     * @throws ConcurrentModificationException
     */
    public Rezervare deleteRezervare(Long id) throws ConcurrentModificationException {
        Rezervare Rezervare = repoRezervare.delete(id);
        //validatorMembruService.validateDelete(membru);
        if(Rezervare != null){
            notifyObservers(new SchimbareEvent());
        }

        return Rezervare;
    }


    public Iterable<Rezervare> getAll(){
        return repoRezervare.findAll();
    }
    public Rezervare findOne(Long id){
        return repoRezervare.findOne(id);
    }


    @Override
    public void addObserver(Observer<SchimbareEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<SchimbareEvent> e) {
        //observers.remove(e);
    }

    @Override
    public void notifyObservers(SchimbareEvent schimbareStareEventChangeEvent) {
        observers.forEach(obs -> obs.update(schimbareStareEventChangeEvent));
    }
}
