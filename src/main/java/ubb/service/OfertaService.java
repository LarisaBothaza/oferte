package ubb.service;

import ubb.domain.Oferta;
import ubb.repository.Repository;
import ubb.utils.events.SchimbareEvent;
import ubb.utils.observer.Observable;
import ubb.utils.observer.Observer;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class OfertaService implements Observable<SchimbareEvent> {
    private final Repository<Long, Oferta> repoOferta;
    private List<Observer<SchimbareEvent>> observers = new ArrayList<>();

    public OfertaService(Repository<Long, Oferta> repoOferta) {
        this.repoOferta = repoOferta;
    }

    /**
     *saves the user received parameter
     * @param MembruParam
     * @return the user created
     */
    public Oferta addOferta(Oferta MembruParam) {
        Oferta oferta = repoOferta.save(MembruParam);
        //validatorMembruService.validateAdd(Membru);

        notifyObservers(new SchimbareEvent());

        return oferta;
    }

    ///TO DO: add other methods

    /**
     *delete the user identified by the received id, and also delete that user's friendships
     * @param id long - id's to be deleted
     * @return deleted user
     * @throws ConcurrentModificationException
     */
    public Oferta deleteOferta(Long id) throws ConcurrentModificationException {
        Oferta oferta = repoOferta.delete(id);
        //validatorMembruService.validateDelete(membru);
        if(oferta != null){
            notifyObservers(new SchimbareEvent());
        }

        return oferta;
    }


    public Iterable<Oferta> getAll(){
        return repoOferta.findAll();
    }
    public Oferta findOne(Long id){
        return repoOferta.findOne(id);
    }


    @Override
    public void addObserver(Observer<SchimbareEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<SchimbareEvent> e) {
        //observers.remove(e);
    }

    @Override
    public void notifyObservers(SchimbareEvent schimbareStareEventChangeEvent) {
        observers.forEach(obs -> obs.update(schimbareStareEventChangeEvent));
    }
}
