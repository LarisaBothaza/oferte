package ubb.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ubb.config.ApplicationContext;

import ubb.controller.IntroductionController;
import ubb.domain.Oferta;
import ubb.domain.Rezervare;
import ubb.repository.Repository;
import ubb.repository.file.OfertaFile;
import ubb.repository.file.RezervareFile;
import ubb.service.OfertaService;
import ubb.service.RezervareService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainFX extends Application {

    private static OfertaService ofertaService ;
    private static RezervareService rezervareService;
    private static List<String> argumente = new ArrayList<>();


    @Override
    public void start(Stage primaryStage) throws Exception {
        initView(primaryStage);

        //primaryStage.show();
    }

    public static void main(String[] args) {
        //configurations

        String fileNameOferta=ApplicationContext.getPROPERTIES().getProperty("data.ubb.oferte");
        String fileNameRezervare=ApplicationContext.getPROPERTIES().getProperty("data.ubb.rezervari");

        //repositories
        Repository<Long, Oferta> ofertaFileRepository = new OfertaFile(fileNameOferta);
        Repository<Long, Rezervare> rezervareFileRepository = new RezervareFile(fileNameRezervare);
        //String fileName="data/users.csv";

        //services
        ofertaService = new OfertaService(ofertaFileRepository);
        rezervareService = new RezervareService(rezervareFileRepository);

        for(int i = 0; i < args.length; i++){
            argumente.add(args[i]);
        }

        launch(args);
    }

    private void initView(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/introduction.fxml"));
        AnchorPane layout = loader.load();
        primaryStage.setScene(new Scene((layout)));

        IntroductionController introductionController = loader.getController();
        introductionController.setOfertaService(ofertaService,argumente);
        introductionController.setRezervareService(rezervareService);

        introductionController.setIntroductionstage(primaryStage);

    }
}

