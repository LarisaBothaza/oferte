package ubb.domain;

public class Rezervare extends Entity<Long>{
    private static long idMax=0;
    private String destinatie;
    private String hotel;
    private String perioada;
    private float pret;
    private int locuriDorite;
    private String numeClient;
    private String adresa;

    public Rezervare(String destinatie, String hotel, String perioada, float pret, int locuriDorite, String numeClient, String adresa) {
        this.destinatie = destinatie;
        this.hotel = hotel;
        this.perioada = perioada;
        this.pret = pret;
        this.locuriDorite = locuriDorite;
        this.numeClient = numeClient;
        this.adresa = adresa;
        idMax++;
        setId(idMax);
    }

    public String getDestinatie() {
        return destinatie;
    }

    public void setDestinatie(String destinatie) {
        this.destinatie = destinatie;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public String getPerioada() {
        return perioada;
    }

    public void setPerioada(String perioada) {
        this.perioada = perioada;
    }

    public float getPret() {
        return pret;
    }

    public void setPret(float pret) {
        this.pret = pret;
    }

    public int getLocuriDorite() {
        return locuriDorite;
    }

    public void setLocuriDorite(int locuriDorite) {
        this.locuriDorite = locuriDorite;
    }

    public String getNumeClient() {
        return numeClient;
    }

    public void setNumeClient(String numeClient) {
        this.numeClient = numeClient;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    @Override
    public String toString() {
        return "Rezervare{" +
                "destinatie='" + destinatie + '\'' +
                ", hotel='" + hotel + '\'' +
                ", perioada='" + perioada + '\'' +
                ", pret=" + pret +
                ", locuriDorite=" + locuriDorite +
                ", numeClient='" + numeClient + '\'' +
                ", adresa='" + adresa + '\'' +
                '}';
    }


}
