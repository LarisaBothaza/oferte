package ubb.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import ubb.domain.Oferta;
import ubb.domain.Rezervare;
import ubb.service.OfertaService;
import ubb.service.RezervareService;
import ubb.utils.events.SchimbareEvent;
import ubb.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;


public class OfertaController implements Observer<SchimbareEvent> {
    ObservableList<Oferta> modelOferte = FXCollections.observableArrayList();
    ObservableList<Oferta> modelOferteFiltrate = FXCollections.observableArrayList();
    OfertaService ofertaService;
    RezervareService rezervareService;
    Stage ofertaStage;

    @FXML
    TableView<Oferta> tableViewOferte;

    @FXML
    TableColumn<Oferta, String> tableColumnDestinatie;

    @FXML
    TableColumn<Oferta, String> tableColumnHotel;

    @FXML
    TableColumn<Oferta, String> tableColumnPerioada;

    @FXML
    TableColumn<Oferta, Float> tableColumnPret;
    @FXML
    TableColumn<Oferta, Integer> tableColumnLocuri;

    @FXML
    TableView<Oferta> tableViewOferteF;

    @FXML
    TableColumn<Oferta, String> tableColumnDestinatieF;

    @FXML
    TableColumn<Oferta, String> tableColumnHotelF;

    @FXML
    TableColumn<Oferta, String> tableColumnPerioadaF;

    @FXML
    TableColumn<Oferta, Float> tableColumnPretF;
    @FXML
    TableColumn<Oferta, Integer> tableColumnLocuriF;

    @FXML
    Button buttonCauta;

    @FXML
    Button buttonRezerva;

    @FXML
    TextField textFieldFiltrare;

    @FXML
    TextField textfieldClient;

    @FXML
    TextField textFieldAdresa;

    @FXML
    TextField textFieldNr;


    @FXML
    public void initialize(){
        tableColumnDestinatie.setCellValueFactory(new PropertyValueFactory<Oferta, String>("Destinatie"));
        tableColumnHotel.setCellValueFactory(new PropertyValueFactory<Oferta, String>("Hotel"));
        tableColumnPerioada.setCellValueFactory(new PropertyValueFactory<Oferta, String>("Perioada"));
        tableColumnPret.setCellValueFactory(new PropertyValueFactory<Oferta, Float>("Pret"));
        tableColumnLocuri.setCellValueFactory(new PropertyValueFactory<Oferta, Integer>("LocuriDisponibile"));
        tableViewOferte.setItems(modelOferte);

        tableColumnDestinatieF.setCellValueFactory(new PropertyValueFactory<Oferta, String>("Destinatie"));
        tableColumnHotelF.setCellValueFactory(new PropertyValueFactory<Oferta, String>("Hotel"));
        tableColumnPerioadaF.setCellValueFactory(new PropertyValueFactory<Oferta, String>("Perioada"));
        tableColumnPretF.setCellValueFactory(new PropertyValueFactory<Oferta, Float>("Pret"));
        tableColumnLocuriF.setCellValueFactory(new PropertyValueFactory<Oferta, Integer>("LocuriDisponibile"));
        tableViewOferteF.setItems(modelOferteFiltrate);

    }


    private void initModel() {
        Iterable<Oferta> oferte = ofertaService.getAll();
        List<Oferta> filter = new ArrayList<>();

        oferte.forEach(o->{
            if(o.getLocuriDisponibile() > 0)
                filter.add(o);
        });

        modelOferte.setAll(filter);
    }

    @Override
    public void update(SchimbareEvent schimbareEvent) {
        initModel();
    }

    public void setAttributes(OfertaService ofertaService, RezervareService rezervareService, Stage ofertaStage) {
        this.ofertaService = ofertaService;
        this.rezervareService = rezervareService;
        this.ofertaStage = ofertaStage;
        this.ofertaService.addObserver(this);
        this.rezervareService.addObserver(this);
        initModel();
    }

    public void filtrareOferte() {
        String text = textFieldFiltrare.getText();

        Iterable<Oferta> oferte = ofertaService.getAll();
        List<Oferta> filter = new ArrayList<>();

        oferte.forEach(o->{
            if(o.getDestinatie().startsWith(text) || o.getDestinatie().contains(text))
                filter.add(o);
        });

        modelOferteFiltrate.setAll(filter);
    }

    public void clearFiltrare(){

        List<Oferta> list = new ArrayList<>();
        modelOferteFiltrate.setAll(list);
        textFieldFiltrare.clear();

    }

    public void rezervare() {
        Oferta oferta = tableViewOferteF.getSelectionModel().getSelectedItem();

        if(oferta != null){
            String nume = textfieldClient.getText();
            String adresa = textFieldAdresa.getText();
            String nrLocuri = textFieldNr.getText();


            int NR = oferta.getLocuriDisponibile() - Integer.parseInt(nrLocuri);
            if(NR >= 0 ){

                Rezervare rezervare = new Rezervare(oferta.getDestinatie(),oferta.getHotel(),
                        oferta.getPerioada(), oferta.getPret(), Integer.parseInt(nrLocuri),nume,adresa);
                rezervareService.addRezervare(rezervare);
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Rezervare cu succes!");
                alert.show();
                textfieldClient.clear();
                textFieldAdresa.clear();
                textFieldNr.clear();
                ofertaService.deleteOferta(oferta.getId());
                if(NR > 0){
                    Oferta ofertaUpdate = new Oferta(oferta.getDestinatie(),oferta.getHotel(),oferta.getPerioada(),oferta.getPret(), NR);
                    ofertaUpdate.setId(oferta.getId());
                    ofertaService.addOferta(ofertaUpdate);
                }
                initModel();
                clearFiltrare();
            }
            else{
                Alert alert = new Alert(Alert.AlertType.ERROR, "Locuri insuficiente!");
                alert.show();
            }
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Nu ati selectat nimic din tabea filtrata!");
            alert.show();
        }
    }
}
