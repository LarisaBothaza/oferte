package ubb.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ubb.domain.Oferta;
import ubb.service.OfertaService;
import ubb.service.RezervareService;
import ubb.utils.events.SchimbareEvent;
import ubb.utils.observer.Observer;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class IntroductionController implements Observer<SchimbareEvent> {
    OfertaService ofertaService;
    RezervareService rezervareService;
    List<String> argumente;

    ObservableList<Oferta> modelOferta = FXCollections.observableArrayList();

    Stage introductionstage;

    @FXML
    public void initialize(){
/*        tableColumnNume.setCellValueFactory(new PropertyValueFactory<Membru, String>("Name"));
        tableColumnRol.setCellValueFactory(new PropertyValueFactory<Membru, String>("Rol"));
        tableColumnStare.setCellValueFactory(new PropertyValueFactory<Membru, String>("Stare"));

        tableViewMembers.setItems(model);*/
    }

    public void setIntroductionstage(Stage introductionstage) {
        //this.introductionstage = introductionstage;


        argumente.forEach(a->{
            showOferteStage(a);
        });
    }

    private void initModel(){

        Iterable<Oferta> oferte = ofertaService.getAll();
        List<Oferta> filter = new ArrayList<>();

        oferte.forEach(o->{
            if(o.getLocuriDisponibile() > 0)
                filter.add(o);
        });

        modelOferta.setAll(filter);
    }

    public void setOfertaService(OfertaService ofertaService, List<String> argumente) {
        this.ofertaService = ofertaService;
        this.argumente = argumente;
        this.ofertaService.addObserver(this);
        initModel();

    }
    public void setRezervareService(RezervareService rezervareService) {

        this.rezervareService = rezervareService;
        this.rezervareService.addObserver(this);
    }

/*    public void selectFriendsUser(){
        UserDTO selectedUserDTO = tableViewUserDTO.getSelectionModel().getSelectedItem();
        //prietenieService.getAllFriendshipsUser(selectedUserDTO.getId()).forEach(System.out::println);
        if(selectedUserDTO != null){
            showAccountUser(selectedUserDTO);

        }

    }*/

    private void showOferteStage(String name){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/ofertaView.fxml"));
            AnchorPane root = loader.load();

            Stage ofertaStage = new Stage();
            ofertaStage.setTitle(name+"'s account");

            Scene scene = new Scene(root);
            ofertaStage.setScene(scene);
            OfertaController ofertaController = loader.getController();

            ofertaController.setAttributes(ofertaService,rezervareService,ofertaStage);

            ofertaStage.show();


        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

/*    public void selectMember(){
        Membru selectedItem = tableViewMembers.getSelectionModel().getSelectedItem();

        //prietenieService.getAllFriendshipsUser(selectedUserDTO.getId()).forEach(System.out::println);
        if(selectedItem != null){
            if(selectedItem.getRol().equals("sef"))
                showAccountSef(selectedItem);
            else
                showAccountMembru(selectedItem);
        }

    }*/


    @Override
    public void update(SchimbareEvent schimbareStareEvent) {
        modelOferta.setAll((Collection<? extends Oferta>) this.ofertaService.getAll());
    }
}
